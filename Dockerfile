FROM alpine:3.5
MAINTAINER Sergio Castaño Arteaga <sergio@ipowow.com>

ADD src /ctx/src

RUN set -x \
    && apk add --no-cache --virtual .build-deps 'go<1.8' git musl-dev \
    && GOPATH=/go go get github.com/constabulary/gb/... \
    && cd /ctx/src/cmd && /go/bin/gb build -ldflags '-linkmode external -extldflags "-static"' \
    && cp /ctx/bin/* /usr/bin \
    && rm -rf /go* /ctx \
    && apk del .build-deps

CMD ["/usr/bin/sample-main"]
